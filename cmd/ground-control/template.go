package main

type IpxeParams struct {
	Id          string
	InstallDisk string
	Kernel      string
	Rootfs      string
	Initramfs   string
	Netdev      string
	Mac         string
	Server      string
	Args        string
}

// boot the coreos installer
var ipxeTemplate = `#!ipxe
dhcp
kernel http://{{.Server}}/{{.Kernel}} initrd=initramfs coreos.live.rootfs_url=http://{{.Server}}/{{.Rootfs}} coreos.inst.install_dev={{.InstallDisk}} coreos.inst.ignition_url=http://{{.Server}}/ignition ignition.platform.id={{.Id}} ip={{.Netdev}}:dhcp selinux=0 {{.Args}}
initrd http://{{.Server}}/{{.Initramfs}}
boot`

// boot from the already installed os on disk
var ipxeLocalTemplate = `#!ipxe
sanboot --no-describe --drive 0x80
`

type NftablesParams struct {
	Ifx string
}

var nftablesTemplate = `#!/usr/sbin/nft -F

table ip gc_nat
delete table ip gc_nat

table ip gc_nat {
        chain prerouting {
                type nat hook prerouting priority dstnat; policy accept;
        }

        chain postrouting {
                type nat hook postrouting priority srcnat; policy accept;
                oifname "{{.Ifx}}" masquerade
        }
}
`

var sysctlTemplate = `net.ipv4.ip_forward=1
`
