package main

import (
	"fmt"
	"io"
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"
)

func fetch() error {

	err := fetchIpxe()
	if err != nil {
		return err
	}

	err = fetchCumulus()
	if err != nil {
		return err
	}

	return fetchFCOS()

}

func fetchIpxe() error {

	// FIXME this should really be in the config file
	baseurl := fmt.Sprintf(
		"https://gitlab.com/mergetb/tech/ipxe/-/" +
			"jobs/artifacts/merge/raw/src/bin-ground-control",
	)
	basedir := "/var/ground-control/tftp"

	err := os.MkdirAll(basedir, 0755)
	if err != nil {
		log.Fatalf("failed to create dir %s", basedir)
	}

	url := fmt.Sprintf("%s/snponly.efi?job=build", baseurl)
	outfile := fmt.Sprintf("%s/snponly.efi", basedir)
	err = fetchFile(url, outfile)
	if err != nil {
		return err
	}

	url = fmt.Sprintf("%s/ipxe.pxe?job=build", baseurl)
	outfile = fmt.Sprintf("%s/ipxe.pxe", basedir)
	err = fetchFile(url, outfile)
	if err != nil {
		return err
	}

	basedir = "/var/ground-control/http"
	err = os.MkdirAll(basedir, 0755)
	if err != nil {
		return err
	}

	for _, x := range cfg.StaticFiles {

		err = fetchFile(x.SourceURL,
			fmt.Sprintf("/var/ground-control/http/%s", x.DestFilename))
		if err != nil {
			return err
		}

	}

	return nil

}

func fetchFCOS() error {

	baseurl := fmt.Sprintf(
		"builds.coreos.fedoraproject.org/prod/streams/%s/builds/%s/x86_64",
		fcosStream,
		fcosVersion,
	)
	basedir := "/var/ground-control/fcos"

	err := os.MkdirAll(basedir, 0755)
	if err != nil {
		return fmt.Errorf("create dir %s: %v", basedir, err)
	}

	// kernel
	outfile := fmt.Sprintf("%s/kernel", basedir)
	url := fmt.Sprintf(
		"https://%s/fedora-coreos-%s-live-kernel-x86_64",
		baseurl, fcosVersion,
	)
	err = fetchFile(url, outfile)
	if err != nil {
		return err
	}

	// initramfs
	outfile = fmt.Sprintf("%s/initramfs", basedir)
	url = fmt.Sprintf(
		"https://%s/fedora-coreos-%s-live-initramfs.x86_64.img",
		baseurl, fcosVersion,
	)
	err = fetchFile(url, outfile)
	if err != nil {
		return err
	}

	// rootfs
	outfile = fmt.Sprintf("%s/rootfs", basedir)
	url = fmt.Sprintf(
		"https://%s/fedora-coreos-%s-live-rootfs.x86_64.img",
		baseurl, fcosVersion,
	)
	err = fetchFile(url, outfile)
	if err != nil {
		return err
	}

	return nil

}

func fetchFile(url, dst string) error {

	_, err := os.Stat(dst)
	if err != nil && os.IsNotExist(err) {

		log.Infof("fetching %s -> %s", url, dst)

		out, err := os.Create(dst)
		if err != nil {
			return fmt.Errorf("failed to create %s: %v", dst, err)
		}
		defer out.Close()

		resp, err := http.Get(url)
		if err != nil {
			os.RemoveAll(dst)
			return fmt.Errorf("failed to fetch %s: %v", url, err)
		}

		if resp.StatusCode != 200 {
			os.RemoveAll(dst)
			return fmt.Errorf("failed to fetch %s: status code %d", url, resp.StatusCode)
		}

		log.Debugf("fetched %s successfully", url)

		_, err = io.Copy(out, resp.Body)
		if err != nil {
			os.RemoveAll(dst)
			return fmt.Errorf("failed to copy to %s: %v", dst, err)
		}
	}

	return nil

}

func fetchCumulus() error {

	basedir := "/var/ground-control/http"

	err := os.MkdirAll(basedir, 0755)
	if err != nil {
		return fmt.Errorf("create dir %s: %v", basedir, err)
	}

	var url string
	if cfg.OnieURL == "" {
		url = "https://storage.googleapis.com/3p-content-mergetb-dev/cumulus-linux-4.3.0-vx-amd64.bin"
	} else {
		url = cfg.OnieURL
	}
	outfile := fmt.Sprintf("%s/onie-installer-x86_64", basedir)

	return fetchFile(url, outfile)

}
