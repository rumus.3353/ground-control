package main

import (
	"bytes"
	"fmt"
	"text/template"

	"github.com/lorenzosaino/go-sysctl"
)

func setupNat(ifx string) error {

	err := setupNftables(ifx)
	if err != nil {
		return err
	}

	err = setupSysctls()
	if err != nil {
		return err
	}

	return nil

}

func setupNftables(ifx string) error {

	// setup nftables

	tmpl, err := template.New("nftables").Parse(nftablesTemplate)
	if err != nil {
		return fmt.Errorf("template create error: %v", err)
	}

	cfg := NftablesParams{Ifx: ifx}

	buf := new(bytes.Buffer)

	err = tmpl.Execute(buf, cfg)
	if err != nil {
		return fmt.Errorf("failed to execute nftables template: %v", err)
	}

	return nil

}

func setupSysctls() error {

	return sysctl.Set("net.ipv4.ip_forward", "1")

}
