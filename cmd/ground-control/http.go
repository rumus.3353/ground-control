package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"text/template"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	groundcontrol "gitlab.com/mergetb/ops/ground-control/pkg/api"
)

func runHttp() {

	e := echo.New()
	e.HideBanner = true
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Static("/", "/var/ground-control/http")
	// CoreOS 37+ installer uses the HEAD method, which e.Static() does not support
	e.Group("/fcos", middleware.Static("/var/ground-control/fcos"))
	e.GET("/ipxe", ipxeHandler)
	e.GET("/ignition", ignitionHandler)
	e.GET("/ztp", ztpHandler)

	e.Logger.Fatal(e.Start(bindhttp))

}

func getNodeConfig(c echo.Context) (*groundcontrol.NodeConfig, error) {

	addr := strings.Split(c.Request().RemoteAddr, ":")[0]

	// the address might be forwarded if ground-control is running behind a load balancer or so
	forward := strings.TrimSpace(c.Request().Header.Get("x-forwarded-for"))
	if forward != "" {
		addr = strings.TrimSpace(strings.Split(forward, ",")[0])
	}

	var nc *groundcontrol.NodeConfig
	for _, x := range cfg.Nodes {
		if x.Ipv4 == addr {
			nc = x.NodeConfig
		}
	}
	if nc == nil {
		return nil, fmt.Errorf("remote ip %s not found", addr)
	}

	return nc, nil

}

func getSwitchConfig(c echo.Context) (*groundcontrol.SwitchConfig, error) {

	addr := strings.Split(c.Request().RemoteAddr, ":")[0]

	// the address might be forwarded if ground-control is running behind a load balancer or so
	forward := strings.TrimSpace(c.Request().Header.Get("x-forwarded-for"))
	if forward != "" {
		addr = strings.TrimSpace(strings.Split(forward, ",")[0])
	}

	var sc *groundcontrol.SwitchConfig
	for _, x := range cfg.Switches {
		if x.Ipv4 == addr {
			sc = x.SwitchConfig
		}
	}
	if sc == nil {
		return nil, fmt.Errorf("remote ip %s not found", addr)
	}

	return sc, nil

}

func ipxeHandler(c echo.Context) error {

	nc, err := getNodeConfig(c)
	if err != nil {
		return nil
	}

	var t string
	switch nc.Boot {
	case groundcontrol.BootState_INSTALL:
		t = ipxeTemplate
		nc.Boot = groundcontrol.BootState_DISK // boot from disk after install
	case groundcontrol.BootState_DISK:
		t = ipxeLocalTemplate
	default:
	}

	tmpl, err := template.New("ipxe").Parse(t)
	if err != nil {
		return fmt.Errorf("template create error: %v", err)
	}

	cfg := IpxeParams{
		Id: "metal",
		InstallDisk: nc.Installdisk,
		Kernel:      "fcos/kernel",
		Rootfs:      "fcos/rootfs",
		Initramfs:   "fcos/initramfs",
		Netdev:      nc.Netdev,
		Mac:         nc.Mac,
		Args:        nc.Pxeargs,
		Server:      danceaddrv4,
	}

	var out bytes.Buffer
	err = tmpl.Execute(&out, cfg)
	if err != nil {
		return fmt.Errorf("template exec error: %v", err)
	}

	return c.String(http.StatusOK, out.String())
}

func ignitionHandler(c echo.Context) error {

	nc, err := getNodeConfig(c)
	if err != nil {
		return nil
	}

	buf, err := ioutil.ReadFile(nc.IgnitionFile())
	if err != nil {
		return fmt.Errorf(
			"read ignition config file %s: %v", nc.IgnitionFile(), err)
	}

	return c.String(http.StatusOK, string(buf))
}

func ztpHandler(c echo.Context) error {

	sc, err := getSwitchConfig(c)
	if err != nil {
		return nil
	}

	buf, err := ioutil.ReadFile(sc.ZtpFile())
	if err != nil {
		return fmt.Errorf("read ztp file %s: %v", sc.ZtpFile(), err)
	}

	return c.String(http.StatusOK, string(buf))

}
