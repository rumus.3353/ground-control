package groundcontrol

import (
	"fmt"
	"strings"
)

const (
	GRPCEndpoint = 6000
	HTTPEndpoint = 9000
)

func (n *NodeConfig) IgnitionFile() string {

	return "/var/ground-control/ignition/" + n.Ignition

}

func (n *NodeConfig) FirmwareFile() string {

	filename := ""
	if n.Firmware == PxeFirmware_LEGACY {
		filename = "ipxe.pxe"
	} else {
		filename = "snponly.efi"
	}

	return "/var/ground-control/tftp/" + filename

}

func (n *SwitchConfig) ZtpFile() string {

	return "/var/ground-control/ztp/" + n.Ztp

}

func (f PxeFirmware) MarshalYAML() (interface{}, error) {

	name, ok := PxeFirmware_name[int32(f.Number())]
	if !ok {
		return nil, fmt.Errorf("cannot marshal PxeFirmware value: %d", f)
	}

	return name, nil

}

func (f *PxeFirmware) UnmarshalYAML(unmarshal func(interface{}) error) error {

	var firmwareint int32
	err := unmarshal(&firmwareint)
	if err == nil {
		*f = PxeFirmware(firmwareint)
		return nil
	}

	var firmwarestr string
	err = unmarshal(&firmwarestr)
	if err == nil {
		v, ok := PxeFirmware_value[strings.ToUpper(firmwarestr)]
		if !ok {
			return fmt.Errorf("cannot unmarshal PxeFirmware value: %s", firmwarestr)
		}

		*f = PxeFirmware(v)
		return nil
	}

	return fmt.Errorf("cannot unmarshal PxeFirmware value: do not recognize type")

}
