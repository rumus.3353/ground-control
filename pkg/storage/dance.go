package storage

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"os"

	log "github.com/sirupsen/logrus"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/facility/mars/pkg/dance"
)

var (
	dance_storage_dir string = fmt.Sprintf("%s/dance", storage_dir)
	dance_file        string = fmt.Sprintf("%s/dance.json", dance_storage_dir)
)

type DanceData struct {
	// in memory reverse map of name to MAC address
	rmap map[string]uint64

	// in storage
	entries map[uint64]*facility.DanceEntry
}

func NewDanceData() (*DanceData, error) {
	d := new(DanceData)

	err := d.load()
	if err == nil {
		return d, nil
	}

	d.rmap = make(map[string]uint64)
	d.entries = make(map[uint64]*facility.DanceEntry)

	// populate file
	err = d.store()
	if err != nil {
		return nil, err
	}

	return d, nil
}

func (d *DanceData) AddEntry(mac net.HardwareAddr, entry *facility.DanceEntry) error {
	// update entries
	macUint := dance.MacAsUint64(mac)
	if _, ok := d.entries[macUint]; ok {
		log.Warnf("Updating MAC %s in dance storage", mac)
	}

	d.entries[macUint] = entry

	// update storage
	err := d.store()
	if err != nil {
		return err
	}

	// update rmap
	for _, name := range entry.Names {
		d.rmap[name] = macUint
	}

	return nil
}

func (d *DanceData) GetEntry(mac net.HardwareAddr) *facility.DanceEntry {
	macUint := dance.MacAsUint64(mac)
	if v, ok := d.entries[macUint]; ok {
		return v
	}
	return nil
}

func (d *DanceData) RemoveEntry(mac net.HardwareAddr) error {
	var entry *facility.DanceEntry
	var ok bool

	macUint := dance.MacAsUint64(mac)
	if entry, ok = d.entries[macUint]; !ok {
		return fmt.Errorf("mac %s not in dance storage", mac)
	}

	delete(d.entries, macUint)

	// update storage
	err := d.store()
	if err != nil {
		return err
	}

	// update rmap
	for _, name := range entry.Names {
		delete(d.rmap, name)
	}

	return nil
}

// return MAC for the given name
func (d *DanceData) GetMacByName(name string) (net.HardwareAddr, error) {
	var mac uint64
	var ok bool

	// query rmap for the MAC
	if mac, ok = d.rmap[name]; !ok {
		return nil, fmt.Errorf("name %s not in dance storage", name)
	}

	return dance.Uint64AsMac(mac), nil
}

// return a copy of all entries in storage
func (d *DanceData) GetEntries() (map[uint64]*facility.DanceEntry, error) {
	entries := make(map[uint64]*facility.DanceEntry)
	for k, v := range d.entries {
		entries[k] = v
	}
	return entries, nil
}

func (d *DanceData) load() error {
	blob, err := ioutil.ReadFile(dance_file)
	if err != nil {
		return fmt.Errorf("readfile %s: %v", dance_file, err)
	}

	err = json.Unmarshal(blob, &d.entries)
	if err != nil {
		return fmt.Errorf("json unmarshal: %v", err)
	}

	d.rmap = make(map[string]uint64)
	for mac, entry := range d.entries {
		for _, name := range entry.Names {
			d.rmap[name] = mac
		}
	}

	return nil
}

func (d *DanceData) store() error {
	err := os.MkdirAll(dance_storage_dir, 0755)
	if err != nil {
		return err
	}

	blob, err := json.Marshal(d.entries)
	if err != nil {
		return fmt.Errorf("json marshal: %v", err)
	}

	err = ioutil.WriteFile(dance_file, []byte(blob), 0644)
	if err != nil {
		return fmt.Errorf("writefile %s: %v", dance_file, err)
	}

	return nil
}
